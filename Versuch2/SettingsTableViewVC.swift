//
//  SettingsTableViewVC.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 21.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import Firebase

class SettingsTableViewVC: UITableViewController {
    
    @IBOutlet weak var accountTypeLabel: UILabel!
    @IBOutlet weak var pricingLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var negotiationBasisSwitch: UISwitch!
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func negotiationBasisSwitchPressed(_ sender: UISwitch) {
        print("negotiationBasis was changed")
    }
    
    @IBAction func notificationSwitchPressed(_ sender: UISwitch) {
        print("notificationSwitch was changed")
    }
    

}
