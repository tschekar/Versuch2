//
//  tabBarController.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 20.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase

class tabBarControllerMain: UITabBarController {
    
    var userID: String?
    var accounType : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIfUserIsLoggedIn()
    }
    
    @objc func handleNewMessage()
    {
        let messageController = NewMessageTableViewController()
        let navController = UINavigationController(rootViewController: messageController)
        present(navController, animated: true, completion: nil)
    }
    
    func checkFbLoginStatus() -> Bool
    {
        let token = FBSDKAccessToken.current()
        if token != nil {
            return true
        } else {
            return false
        }
    }
    
    func checkIfUserIsLoggedIn()
    {
        
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == false
        {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            
            Auth.auth().signIn(with: credential) { (user, error) in
                if error != nil {
                    print(error!)
                }
            }
            
            return
        }
        
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            let uid = Auth.auth().currentUser?.uid
            Database.database().reference().child("users").child(uid!).observe(.value, with: { (snapshot) in
                
                if let dict = snapshot.value as? [String: AnyObject] {
                    self.navigationItem.title = dict["name"] as? String
                }
                
            }, withCancel: { (error) in
                print(error)
            })
        }
    }
    
    @objc func handleLogout()
    {
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == true
        {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            let loginController = LoginController()
            present(loginController, animated: true, completion: nil)
        }
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
    }


}

extension UIColor {
    convenience init (r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
