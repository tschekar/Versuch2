//
//  ViewController.swift
//  RealTimeChatApp
//
//  Created by Tschekalinskij, Alexander on 17.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class MessagesController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "My Profile", style: .plain, target: self, action: #selector(goToMyProfile))
        
        let newMessageImage = UIImage.init(named: "star")
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: newMessageImage, style: .plain, target: self, action: #selector(handleNewMessage))
        
        checkIfUserIsLoggedIn()
    }
    
    @objc func goToMyProfile()
    {
        
    }
    
    @objc func handleNewMessage()
    {
        let messageController = NewMessageTableViewController()
        let navController = UINavigationController(rootViewController: messageController)
        present(navController, animated: true, completion: nil)
    }
    
    func checkFbLoginStatus() -> Bool
    {
        let token = FBSDKAccessToken.current()
        if token != nil {
            return true
        } else {
            return false
        }
    }
    
    func checkIfUserIsLoggedIn()
    {
        
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == false
        {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            return
        }
        
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            let uid = Auth.auth().currentUser?.uid
            Database.database().reference().child("users").child(uid!).observe(.value, with: { (snapshot) in
                
                if let dict = snapshot.value as? [String: AnyObject] {
                    self.navigationItem.title = dict["name"] as? String
                }
                
            }, withCancel: { (error) in
                print(error)
            })
        }
    }
    
    @objc func handleLogout()
    {
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == true
        {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            let loginController = LoginController()
            present(loginController, animated: true, completion: nil)
        }
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
    }
    
}



