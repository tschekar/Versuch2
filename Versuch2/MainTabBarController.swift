//
//  MainTabBarController.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 20.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if let tabController = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC {
            
            let tabBarItem = UITabBarItem(title: "Menu", image: UIImage(named: "star"), selectedImage: UIImage(named: "star"))
            tabController.tabBarItem = tabBarItem
            var array = self.viewControllers
            array?.append(tabController)
            self.viewControllers = array
            
        }
    }


}
