//
//  MessagesVC.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 20.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class MessagesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var userID: String?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserID()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "My Profile", style: .plain, target: self, action: #selector(goToMyProfile))
    }
    
    func getUserID()
    {
        let token = FBSDKAccessToken.current()
        
        if token == nil {
            self.userID = Auth.auth().currentUser?.uid
        } else {
            self.userID = token?.userID
        }
    }
    
    @objc func goToMyProfile()
    {
        self.tabBarController?.selectedIndex = 3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    @objc func handleLogout()
    {
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == true
        {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            let loginController = LoginController()
            present(loginController, animated: true, completion: nil)
        }
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
    }
    
    func checkFbLoginStatus() -> Bool
    {
        let token = FBSDKAccessToken.current()
        if token != nil {
            return true
        } else {
            return false
        }
    }


}
