//
//  MyProfileVC.swift
//  Versuch2
//
//  Created by Alexander Tschekalinskij on 22.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import CoreLocation
import MapKit

class MyProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    var userID: String?
    var userName: String?
    var rating: Int?
    
    let locationManager = CLLocationManager()
    var mapView = MKMapView()
    var annotation = MKPointAnnotation()
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var ratingStar1: UIImageView!
    @IBOutlet weak var ratingStar2: UIImageView!
    @IBOutlet weak var ratingStar3: UIImageView!
    @IBOutlet weak var ratingStar4: UIImageView!
    @IBOutlet weak var ratingStar5: UIImageView!
    @IBOutlet weak var ratingsStackView: UIStackView!
    @IBOutlet weak var locationButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideEverything()
        showPleaseWaitController()
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.prepareLocationManager()
            self.getUserID()
            self.loadProfilePicture()
            self.readUserInfo()
            self.createRatingsStackView()
            
            DispatchQueue.main.async {
                self.showEverything()
                self.hidePleaseWaitController()
            }
        }

    }
    
    func updateFirebaseUserCoordinates(Lat: Double, Long: Double)
    {
        let values = ["Lat": Lat, "Long": Long]
        let usersReference = Database.database().reference(fromURL: "https://versuch2-a60f8.firebaseio.com/").child("users/").child(self.userID!)
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil {
                print(err!)
                return
            }
            print("User coordinates were updated!")
        })
    }
    
    func convertCoordinateToTown(latitude: Double, longitude: Double)
    {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        var postalCode : String!
        var city : String!
        
        updateFirebaseUserCoordinates(Lat: latitude, Long: longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            city = placeMark.addressDictionary!["City"] as! String
            postalCode = placeMark.addressDictionary!["ZIP"] as! String
            
            let title = "\(postalCode!), \(city!)"
            self.locationButton.setTitle(title, for: .normal)
            print("Coordinates changed to: \(title)")
            
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("User updated coordinates: ")
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        convertCoordinateToTown(latitude: locValue.latitude, longitude: locValue.longitude)
    }
    
    func prepareLocationManager()
    {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.stopUpdatingLocation()
    }
    
    
    func hideEverything()
    {
        self.imageButton.isHidden = true
        self.userNameLabel.isHidden = true
        self.ratingsStackView.isHidden = true
    }
    
    func showEverything()
    {
        self.imageButton.isHidden = false
        self.userNameLabel.isHidden = false
        self.ratingsStackView.isHidden = false
    }
    
    func showPleaseWaitController()
    {
        let alert = UIAlertController(title: nil, message: "Loading Profile Data...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func hidePleaseWaitController()
    {
        dismiss(animated: false, completion: nil)
    }
    
    func createRatingsStackView()
    {
            
            let usersReference = Database.database().reference(fromURL: "https://versuch2-a60f8.firebaseio.com/").child("users/").child(self.userID!)
        
            let myGroup = DispatchGroup()
        
            myGroup.enter()
                usersReference.observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    print(value)
                    self.rating = value?["rating"] as? Int ?? 0
                    myGroup.leave()
                })
        
            myGroup.notify(queue: .main) {
                if self.rating! < 1 {
                    self.ratingStar1.alpha = 0.1
                    self.ratingStar2.alpha = 0.1
                    self.ratingStar3.alpha = 0.1
                    self.ratingStar4.alpha = 0.1
                    self.ratingStar5.alpha = 0.1
                }
                
                if self.rating! >= 1 {
                    self.ratingStar1.alpha = 1
                    self.ratingStar2.alpha = 0.1
                    self.ratingStar3.alpha = 0.1
                    self.ratingStar4.alpha = 0.1
                    self.ratingStar5.alpha = 0.1
                }
                
                if self.rating! >= 2 {
                    self.ratingStar1.alpha = 1
                    self.ratingStar2.alpha = 1
                    self.ratingStar3.alpha = 0.1
                    self.ratingStar4.alpha = 0.1
                    self.ratingStar5.alpha = 0.1
                }
                
                if self.rating! >= 3 {
                    self.ratingStar1.alpha = 1
                    self.ratingStar2.alpha = 1
                    self.ratingStar3.alpha = 1
                    self.ratingStar4.alpha = 0.1
                    self.ratingStar5.alpha = 0.1
                }
                
                if self.rating! >= 4 {
                    self.ratingStar1.alpha = 1
                    self.ratingStar2.alpha = 1
                    self.ratingStar3.alpha = 1
                    self.ratingStar4.alpha = 1
                    self.ratingStar5.alpha = 0.1
                }
                
                if self.rating! == 5 {
                    self.ratingStar1.alpha = 1
                    self.ratingStar2.alpha = 1
                    self.ratingStar3.alpha = 1
                    self.ratingStar4.alpha = 1
                    self.ratingStar5.alpha = 1
                }
            }
    }
    
    func getUserID()
    {

        let token = FBSDKAccessToken.current()
        
        if token == nil {
            self.userID = Auth.auth().currentUser?.uid
        } else {
            self.userID = token?.userID
        }
    }
    
    func readUserInfo()
    {
        let usersReference = Database.database().reference(fromURL: "https://versuch2-a60f8.firebaseio.com/").child("users/").child(self.userID!)
        
        usersReference.observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let username = value?["name"] as? String ?? ""
            
            self.userNameLabel.text = username
        })
    }
    
    @IBAction func imageButtonPressed(_ sender: UIButton) {
        chooseImage()
    }
    
    func chooseImage()
    {
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        } ))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action: UIAlertAction) in imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }  ))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        imageButton.setBackgroundImage(image, for: .normal)
        
        picker.dismiss(animated: true, completion: nil)
        
        updatePhotoAfterPicking()
    }
    
    func updatePhotoAfterPicking()
    {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: "gs://versuch2-a60f8.appspot.com")
        let currentUser = self.userID
        let profilePicRef = storageRef.child(currentUser!+"/profile_pic.jpg")
        
        var img = imageButton.backgroundImage(for: .normal)
        
        img = resizeImage(image: img!, targetSize: CGSize(width: 600.0, height: 600.0))
        
        if img != nil {
            let imgData = UIImagePNGRepresentation(img!) as! NSData
            _ = profilePicRef.putData(imgData as Data, metadata: nil) {
                metadata, error in
                
                if (error == nil)
                {
                    _ = metadata!.downloadURL
                }
                else
                {
                    print("Error in downloading image")
                    print(error!)
                }
            }
        }
        print("Uploaded image to Firebase after picking!")
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func loadProfilePicture()
    {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: "gs://versuch2-a60f8.appspot.com")
        let currentUser = self.userID
        let profilePicRef = storageRef.child(currentUser!+"/profile_pic.jpg")
        
        var userImg : UIImage? = nil
        var imageFB : UIImage? = nil
        
        let myGroup = DispatchGroup()
        myGroup.enter()
        profilePicRef.getData(maxSize: 1 * 1024 * 1024, completion: { (data, error) in
            if (error == nil) {
                DispatchQueue.main.async {
                    imageFB = UIImage(data: data!)
                }
            } else {
                print("There is no image to download yet. Using the standard ghost picture.")
                print(error!)
            }
            myGroup.leave()
        })
        
        myGroup.notify(queue: .main) {
            userImg = imageFB
            if userImg == nil {
                /* If there is no profile pic available */
                userImg = UIImage(named: "not_profile_pic")
            }
            
            //self.imageButton.frame = CGRect(x: self.view.center.x/2 - self.imageButton.frame.width/2, y: 100, width: 160, height: 160)
            self.imageButton.layer.cornerRadius = 0.5 * self.imageButton.bounds.size.width
            self.imageButton.layer.borderColor = UIColor.lightGray.cgColor
            self.imageButton.layer.borderWidth = 1.0
            self.imageButton.clipsToBounds = true
            self.imageButton.setBackgroundImage(userImg, for: .normal)
            //self.imageButton.center = CGPoint(x: self.view.frame.width / 2, y: 150)
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}
