//
//  ChangeLocationVC.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 24.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import MapKit

class ChangeLocationVC: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapKit: MKMapView!
    
    
    var longPressRecognizer = UILongPressGestureRecognizer()
    var annotation = MKPointAnnotation()
    var lat : Double = 0.0
    var long : Double = 0.0
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(DoneButtonPressed))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Current Location", style: .plain, target: self, action: #selector(centerMapOnUserButtonClicked))
        
        prepareLocationManager()
        createMapView()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("User updated coordinates: ")
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        convertCoordinateToTown(latitude: locValue.latitude, longitude: locValue.longitude)
    }
    
    func prepareLocationManager()
    {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.stopUpdatingLocation()
    }
    
    func createMapView()
    {
        
        longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        self.mapKit.addGestureRecognizer(longPressRecognizer)
        
        mapKit.mapType = MKMapType.standard
        mapKit.isZoomEnabled = true
        mapKit.isScrollEnabled = true
        mapKit.showsUserLocation = true
        self.mapKit.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        
        mapKit.center = view.center
    }
    
    @objc func centerMapOnUserButtonClicked() {
        let myGroup = DispatchGroup()
        myGroup.enter()
        
        DispatchQueue.main.async {
            self.mapKit.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        }
        myGroup.leave()
        
        myGroup.notify(queue: .main) {
            let lat = self.mapKit.centerCoordinate.latitude
            let long = self.mapKit.centerCoordinate.longitude
            self.convertCoordinateToTown(latitude: lat, longitude: long)
        }
    }
    
    func convertCoordinateToTown(latitude: Double, longitude: Double)
    {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        var postalCode : String!
        var city : String!
        
        self.lat = latitude
        self.long = longitude
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            city = placeMark.addressDictionary!["City"] as! String
            postalCode = placeMark.addressDictionary!["ZIP"] as! String
            
            let title = "\(postalCode!), \(city!)"
        })
    }
    
    @objc func longPressed(_ sender: UILongPressGestureRecognizer)
    {
        if sender.state == .ended {
            print("Long press detected - do nothing")
        } else if sender.state == .began {
            print("Long press Ended - get new location")
            mapKit.removeAnnotation(annotation)
            let touchPoint = longPressRecognizer.location(in: mapKit)
            let newCoordinates = mapKit.convert(touchPoint, toCoordinateFrom: mapKit)
            annotation.coordinate = newCoordinates
            mapKit.addAnnotation(annotation)
            
            self.lat = newCoordinates.latitude
            self.long = newCoordinates.longitude
            
            convertCoordinateToTown(latitude: newCoordinates.latitude, longitude: newCoordinates.longitude)
        }
    }
    
    @IBAction func DoneButtonPressed() {
        print("Done Button was pressed by user!")
        
//        if delegate != nil {
//            delegate?.didSendLocationData(Lat: self.lat, Long: self.long)
//        }
        
        self.dismiss(animated: true, completion: nil)
    }

}
