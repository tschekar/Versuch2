//
//  User.swift
//  RealTimeChatApp
//
//  Created by Tschekalinskij, Alexander on 19.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit

class User: NSObject {
    var name: String?
    var email: String?
}

