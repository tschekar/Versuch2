//
//  SearchVC.swift
//  Versuch2
//
//  Created by Tschekalinskij, Alexander on 20.04.18.
//  Copyright © 2018 Tschekalinskij, Alexander. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class SearchVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var subjects = ["Maths", "German", "English", "Physics", "Biology", "Chemistry", "History"]
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var subjectsPickerView: UIPickerView!
    @IBOutlet weak var priceSlider: UISlider!
    @IBOutlet weak var distanceSlider: UISlider!
    
    var userID: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserID()
        
        self.subjectsPickerView.delegate = self
        self.subjectsPickerView.dataSource = self
        
        preparePriceAndDistanceLabels()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "My Profile", style: .plain, target: self, action: #selector(goToMyProfile))
        
    }
    
    func getUserID()
    {
        let token = FBSDKAccessToken.current()
        
        if token == nil {
            self.userID = Auth.auth().currentUser?.uid
        } else {
            self.userID = token?.userID
        }
    }
    
    @objc func goToMyProfile()
    {
        self.tabBarController?.selectedIndex = 3
    }
    
    func preparePriceAndDistanceLabels()
    {
        self.priceLabel.text = "Up to 10 Euro"
        self.distanceLabel.text = "Up to 5 km"
    }
    
    @IBAction func priceSliderValueChanged(_ sender: UISlider) {
        let value = Int(self.priceSlider.value)
        print("Price slider changed")
        self.priceLabel.text = "Up to \(value) Euro"
    }
    
    
    @IBAction func distanceSliderValueChanged(_ sender: UISlider) {
        print("Distance slider changed")
        
        let value = Int(self.distanceSlider.value)
        self.distanceLabel.text = "Up to \(value) km"
    }
    
    @objc func handleLogout()
    {
        let fbLoginStatus = checkFbLoginStatus()
        if fbLoginStatus == true
        {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            let loginController = LoginController()
            present(loginController, animated: true, completion: nil)
        }
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
    }
    
    func checkFbLoginStatus() -> Bool
    {
        let token = FBSDKAccessToken.current()
        if token != nil {
            return true
        } else {
            return false
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjects.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjects[row]
    }


}
